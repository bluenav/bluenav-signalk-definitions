module.exports = function (app) {
    var plugin = {};
    var unsubscribes = [];
    let socket;

    plugin.id = 'bluenav-signalk-definitions';
    plugin.name = 'BlueNav Signal K definitions';
    plugin.description = 'BlueNav Signal K definitions plugin for Signal K server';

    plugin.schema = {
        type: 'object',
        properties: {
            // portEngineKey: {
            //     title: 'Port engine name',
            //     type: 'string',
            //     default: 'port',
            // },
            // starboardEngineKey: {
            //     title: 'Starboard engine name',
            //     type: 'string',
            //     default: 'port',
            // },
        },
    };

    plugin.start = function (options) {
        sendUpdate(options);
        return true;
    };

    plugin.stop = function () {
    };

    return plugin;

    function sendUpdate(options) {
        const delta = {
            context: 'vessels.' + app.selfId,
            updates: [
                {
                    meta: [
                        {
                            path: 'electrical.batteries.*.power',
                            value: {
                                description: 'Instantaneous consumed power',
                                units: 'W',
                            },
                        },
                        {
                            path: 'electrical.batteries.*.capacity.estimatedDistanceToEmpty',
                            value: {
                                description: 'Estimated distance to travel before the battery is empty',
                                units: 'm',
                            },
                        },
                        {
                            path: 'propulsion.*.thrust',
                            value: {
                                description: 'Temperature of electric engine',
                                units: 'K',
                            },
                        },
                        {
                            path: 'propulsion.*.temperature',
                            value: {
                                description: 'Temperature of electric engine',
                                units: 'K',
                            },
                        },
                        {
                            path: 'propulsion.*.current',
                            value: {
                                description: 'Current of electric engine',
                                units: 'A',
                            },
                        },
                        {
                            path: 'propulsion.*.voltage',
                            value: {
                                description: 'Voltage of electric engine',
                                units: 'V',
                            },
                        },
                        {
                            path: 'propulsion.*.controller.temperature',
                            value: {
                                description: 'Temperature of electric engine controller',
                                units: 'K',
                            },
                        },
                        {
                            path: 'propulsion.*.controller.current',
                            value: {
                                description: 'Current of electric engine controller',
                                units: 'A',
                            },
                        },
                        {
                            path: 'propulsion.*.controller.voltage',
                            value: {
                                description: 'Voltage of electric engine controller',
                                units: 'V',
                            },
                        },
                    ],
                },
            ],
        };
        app.debug('delta: ' + JSON.stringify(delta));
        app.handleMessage(plugin.id, delta);
    }
};
